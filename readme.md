### Marco Antonio Xocop Roquel

## 201122934

### git clone

Comando para descargar codigo fuente existente desde un repositorio, realiza una copia exacta y la guarda de forma local.

`git clone <https://link-con-nombre-del-repositorio>`

### git branch
Las ramas (branch) se utilizan para trabajar en paralelo en el mismo proyecto. Son altamamente utilizadas.

`git branch <nombre-de-la-rama>`

### git checkout
Se utiliza para trabajar en ramas, para usarla debemos entrar en una de ellas, su fin principal es chequear commits y archivos.

`git checkout <nombre-de-la-rama>`
